<?php

 /*
  * blog actions
  */
    
  // Setup Theme
  add_action('after_setup_theme', 'bwh_theme_setup');
  
  // Enqueue Theme scripts in footer
  add_action( 'wp_enqueue_scripts', 'bwh_theme_enqueue_script');  
  
  // Enqueue Theme styles
  add_action( 'wp_enqueue_scripts', 'bwh_theme_enqueue_style');
  
  // Disable support for comments and trackbacks in post types
  add_action('admin_init', 'bwh_disable_comments_post_types_support');
  
  // Remove comments page in menu
  add_action('admin_menu', 'bwh_disable_comments_admin_menu');
  
  // Redirect any user trying to access comments page
  add_action('admin_init', 'bwh_disable_comments_admin_menu_redirect');
  
  // Remove comments metabox from dashboard
  add_action('admin_init', 'bwh_disable_comments_dashboard');

  // Remove comments links from admin bar
  add_action('init', 'bwh_disable_comments_admin_bar');
  
  // Remove logo
  add_action('wp_before_admin_bar_render', 'remove_wp_logo', 999 );
  
  // Custom login logo
  add_action('login_head', 'custom_login_logo');
  
  // Add quicksearch for users
  add_action('wp_ajax_search', 'quicksearch');
  
  // Add quicksearch for regular users
  add_action('wp_ajax_nopriv_search', 'quicksearch');
  
  // Rewrite search
  add_action( 'template_redirect', 'fb_change_search_url_rewrite' );  
  
  // Add custom editor style
  add_action( 'admin_init', 'bwh_add_editor_styles' );
  
  
  // Remove category from url
  add_action( 'created_category',   'remove_category_url_refresh_rules' );

  add_action( 'delete_category',    'remove_category_url_refresh_rules' );

  add_action( 'edited_category',    'remove_category_url_refresh_rules' );

  add_action( 'init',               'remove_category_url_permastruct' );
  
  add_action( 'after_switch_theme', 'remove_category_url_refresh_rules', 10 ,  2);
  
  add_action( 'switch_theme',       'remove_category_url_deactivate', 10 , 2);
  //
  
 /*
 ACF Google Maps API key
 */
add_filter('acf/settings/google_api_key', function () {
	return 'AIzaSyBS4RJXmVMhhaYIy2FMs0uN5R9sJWZUbms';
});

 /*
  * blog filters
  */  
  
  // Hide adminbar on Front
  add_filter('show_admin_bar', '__return_false');
  
  // Force paste as text
  add_filter('tiny_mce_before_init', 'bwh_tinymce_paste_as_text');
  
  // Remove anchor from more link
  add_filter( 'the_content_more_link', 'remove_more_link_scroll' );
  
  
  
  
  
  // Remove category from url
  add_filter( 'category_rewrite_rules', 'remove_category_url_rewrite_rules' );
  
  add_filter( 'query_vars',             'remove_category_url_query_vars' );
  
  add_filter( 'request',                'remove_category_url_request' ); 
  //
  
  function custom_login_logo() {
      
    echo '<style type="text/css">
              .login h1 a { background-image: url('.get_template_directory_uri() . '/dist/img/login_logo.png) !important; width: 211px; height: 117px; background-size: 211px 117px; }
            </style>';
  }
  
  function bwh_add_editor_styles() {
    add_editor_style( 'editor-style.css' );
}


 /*
  * Register menu
  */
  
  register_nav_menus(array(
      'primary_navigation' => __('Hoofdmenu', 'theme'),
      'mobile_navigation' => __('Mobielmenu', 'theme')
  ));
  
  
 /*
  * blog functions
  */
  
  function bwh_theme_setup() {

    add_theme_support(
      'custom-header',
      Array(
        'width'         => 1280,
        'height'        => 720,
        'default-image' => get_template_directory_uri() . '/dist/img/feature.jpg',
        'uploads'       => true,
        'header-text'   => false
      )
    );
    
    add_theme_support(
      'post-thumbnails'
    );
    
  }  

  
 /*
  * Register theme scripts
  */

  function bwh_theme_enqueue_script() {
    
    // Register jQuery for front-end
    if( !is_admin()){
      //wp_dequeue_style( 'jquery' );
  	  //wp_deregister_script('jquery');
      //wp_register_script(
      //  'jquery',
      //  '//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js',
      //  false,
      //  '1.11.2',
      //  true
      //);
      //wp_enqueue_script('jquery');
    }
        
    // Register theme script
    wp_register_script(
      'celebratingdiversity', 
      get_template_directory_uri() . '/dist/js/CelebratingDiversity.min.js',
      array( 'jquery' ),
      '1.0', 
      true
    );

	// Register facebook count script
    wp_register_script(
      'bwhfacebook', 
      get_template_directory_uri() . '/dist/js/bwh.fb.js',
      array( 'jquery' ),
      '1.0', 
      true
    );
    
	// Register facebook count script
    wp_register_script(
      'waypoints', 
      get_template_directory_uri().'/dist/js/jquery.waypoints.min.js',
      array( 'jquery' ),
      '1.0', 
      true
    );

    // Register matchheight script
    wp_register_script(
      'matchheight', 
      get_template_directory_uri() . '/dist/js/matchheight.min.js',
      array( 'jquery' ),
      '1.0', 
      true
    );
       
	wp_enqueue_script('bwhfacebook');
    wp_enqueue_script('matchheight');
    wp_enqueue_script('waypoints');
    wp_enqueue_script('celebratingdiversity');
    
    wp_localize_script('celebratingdiversity', 'ajax_object', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ), 'themeurl' => get_stylesheet_directory_uri()));
    
  };
  
 /*
  * Register theme styles
  */
  
  function bwh_theme_enqueue_style() {
    
    // Register fonts.com font
    wp_register_style(
      'foundryfont',
      '//fast.fonts.net/cssapi/e66b6733-75b5-4b0b-b959-7080a152e667.css',
      false,
      '1.0',
      'all'
    );
    
    // Register theme style
    wp_register_style(
      'default',
      get_template_directory_uri() . '/dist/css/screen.min.css',
      false,
      '1.0',
      'all'
    );
    
    wp_enqueue_style( 'foundryfont' );
    wp_enqueue_style( 'default' );
    
  }
  
 /*
  * Remove anchorlink
  */
  function remove_more_link_scroll( $link ) {
	  $link = preg_replace( '|#more-[0-9]+|', '', $link );
    return $link;
  }

 /*
  * Disable comment post type
  */
      
  function bwh_disable_comments_post_types_support() {
  	$post_types = get_post_types();
  	foreach ($post_types as $post_type) {
  		if(post_type_supports($post_type, 'comments')) {
  			remove_post_type_support($post_type, 'comments');
  			remove_post_type_support($post_type, 'trackbacks');
  		}
  	}
  }

 /*
  * Disable comment status
  */

  function bwh_disable_comments_status() {
	 return false;
  }

 /*
  * Hide existing comments
  */
  
  function bwh_disable_comments_hide_existing_comments($comments) {
	  $comments = array();
    return $comments;
  }

 /*
  * Hide comments from admin menu
  */
  
  function bwh_disable_comments_admin_menu() {
	  remove_menu_page('edit-comments.php');
  }

/*
	Enable ACF option fields 
	*/
	if( function_exists('acf_add_options_page') ) { 
	    acf_add_options_page(array(
			'page_title' 	=> 'Configuratie',
			'menu_title'	=> 'Configuratie',
			'menu_slug' 	=> 'theme-general-settings',
			'capability'	=> 'edit_posts',
			'icon_url'		=> 'dashicons-admin-site',
			'redirect'		=> true
		));
	    acf_add_options_sub_page(array(
			'page_title' 	=> 'Algemene instellingen',
			'menu_title'	=> 'Algemeen',
			'parent_slug'	=> 'theme-general-settings',
			'capability'	=> 'edit_posts',
		));
		acf_set_options_page_capability( 'edit_posts' );
	} else {
		add_action( 'admin_notices', 'acf_admin_notice__error' );
		function acf_admin_notice__error() {
		    ?>
		    <div class="notice notice-error is-dismissible">
		        <p><?php _e( 'Advanced Custom Fields PRO plugin is niet geïnstalleerd.', TEXTDOMAIN ); ?></p>
		    </div>
		    <?php
		}
	}
	
 /*
  * Redirect direct access to comments
  */

  function bwh_disable_comments_admin_menu_redirect() {
	  global $pagenow;
    if ($pagenow === 'edit-comments.php') {
		  wp_redirect(admin_url()); exit;
	  }
  }

 /*
  * Disable comments dashboard and some other clutter
  */
   
  function bwh_disable_comments_dashboard() {
  	remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
  	remove_meta_box('dashboard_quick_press', 'dashboard', 'normal');
  	remove_meta_box('dashboard_right_now', 'dashboard', 'normal');
  	remove_meta_box('dashboard_primary', 'dashboard', 'normal');
  	remove_action( 'welcome_panel', 'wp_welcome_panel' );
  }

 /*
  * Remove comments from admin bar
  */

  function bwh_disable_comments_admin_bar() {
  	if (is_admin_bar_showing()) {
  		remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
  	}
  }

 /*
  * Hide wordpress logo
  */
  
  function remove_wp_logo( $wp_admin_bar ) {
    global $wp_admin_bar;
  	$wp_admin_bar->remove_menu('wp-logo');
  	$wp_admin_bar->remove_node( 'comments' );
  }
  
 /*
  * Force tinymce to paste as text
  */  
  
  function bwh_tinymce_paste_as_text( $init ) {
    $init['paste_as_text'] = true;
    return $init;
  }
  
  function fb_change_search_url_rewrite() {
	if ( is_search() && ! empty( $_GET['s'] ) ) {
		wp_redirect( home_url( "/search/" ) . urlencode( get_query_var( 's' ) ) );
		exit();
	}	
}
  
  /*
   * Quicksearch
   */

  function quicksearch() {
    
    global $wpdb;
  	global $wp_query;
  	
  	$relevanssiQuery = new WP_Query();
  	$wp_query->query_vars['posts_per_page'] = 10;
  	$relevanssiQuery->query_vars = $wp_query->query_vars;
  	$relevanssiQuery->query_vars['s'] = $_POST['s'];	
  	relevanssi_do_query($relevanssiQuery);
    
    echo "<ul>";
      
    if ( $relevanssiQuery->have_posts() ) :
                  
      while ( $relevanssiQuery->have_posts() ) : $relevanssiQuery->the_post();
      
        echo '<li><a href="'.get_the_permalink().'">'.get_the_title().'</a></li>';
      
      endwhile;
      
    endif;
    
    echo "</ul>";
      
    exit(); 
  }
  
  /**
 * Disable the emoji's
 */
function disable_emojis() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );	
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );	
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
}
add_action( 'init', 'disable_emojis' );

/**
 * Filter function used to remove the tinymce emoji plugin.
 * 
 * @param    array  $plugins  
 * @return   array             Difference betwen the two arrays
 */
function disable_emojis_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}

function remove_category_url_refresh_rules() {
	global $wp_rewrite;
	$wp_rewrite->flush_rules();
}

function remove_category_url_deactivate() {
	remove_filter( 'category_rewrite_rules', 'remove_category_url_rewrite_rules' ); // We don't want to insert our custom rules again
	remove_category_url_refresh_rules();
}

/**
 * Removes category base.
 *
 * @return void
 */
function remove_category_url_permastruct() {
	global $wp_rewrite, $wp_version;

	$wp_rewrite->extra_permastructs['category'][0] = '%category%';
	
}

/**
 * Adds our custom category rewrite rules.
 *
 * @param  array $category_rewrite Category rewrite rules.
 *
 * @return array
 */
function remove_category_url_rewrite_rules( $category_rewrite ) {
	global $wp_rewrite;

	$category_rewrite = array();

	/* WPML is present: temporary disable terms_clauses filter to get all categories for rewrite */
	if ( class_exists( 'Sitepress' ) ) {
		global $sitepress;

		remove_filter( 'terms_clauses', array( $sitepress, 'terms_clauses' ) );
		$categories = get_categories( array( 'hide_empty' => false, '_icl_show_all_langs' => true ) );
		add_filter( 'terms_clauses', array( $sitepress, 'terms_clauses' ) );
	} else {
		$categories = get_categories( array( 'hide_empty' => false ) );
	}

	foreach ( $categories as $category ) {
		$category_nicename = $category->slug;
		if (  $category->parent == $category->cat_ID ) {
			$category->parent = 0;
		} elseif ( 0 != $category->parent ) {
			$category_nicename = get_category_parents(  $category->parent, false, '/', true  ) . $category_nicename;
		}
		$category_rewrite[ '(' . $category_nicename . ')/(?:feed/)?(feed|rdf|rss|rss2|atom)/?$' ] = 'index.php?category_name=$matches[1]&feed=$matches[2]';
		$category_rewrite[ '(' . $category_nicename . ')/page/?([0-9]{1,})/?$' ] = 'index.php?category_name=$matches[1]&paged=$matches[2]';
		$category_rewrite[ '(' . $category_nicename . ')/?$' ] = 'index.php?category_name=$matches[1]';
	}

	// Redirect support from Old Category Base
	$old_category_base = get_option( 'category_base' ) ? get_option( 'category_base' ) : 'category';
	$old_category_base = trim( $old_category_base, '/' );
	$category_rewrite[ $old_category_base . '/(.*)$' ] = 'index.php?category_redirect=$matches[1]';

	return $category_rewrite;
}

function remove_category_url_query_vars( $public_query_vars ) {
	$public_query_vars[] = 'category_redirect';

	return $public_query_vars;
}

/**
 * Handles category redirects.
 *
 * @param $query_vars Current query vars.
 *
 * @return array $query_vars, or void if category_redirect is present.
 */
function remove_category_url_request( $query_vars ) {
	if ( isset( $query_vars['category_redirect'] ) ) {
		$catlink = trailingslashit( get_option( 'home' ) ) . user_trailingslashit( $query_vars['category_redirect'], 'category' );
		status_header( 301 );
		header( "Location: $catlink" );
		exit;
	}

	return $query_vars;
}


/*
Add custom endpoint to inruilen pagina (to include motor slug)
*/
add_filter( 'query_vars', 'bwh_custom_query_vars');
function bwh_custom_query_vars($vars){
	$vars[] = "filter";
	return $vars;
}
add_action('init', 'bwh_custom_rewrites');
function bwh_custom_rewrites() {

	add_rewrite_rule('agenda/filter/?([^/]*)/?$', 			'index.php?post_type=agenda&filter=$matches[1]', 'top');
	add_rewrite_rule('en/activities/filter/?([^/]*)/?$', 	'index.php?post_type=agenda&filter=$matches[1]', 'top');
}

// Register Custom Post Type
function bwh_register_posttypes() {

	$labels = array(
		'name'                  => _x( 'Activiteiten', 'Post Type General Name', 'bwhontwerpers' ),
		'singular_name'         => _x( 'Activiteit', 'Post Type Singular Name', 'bwhontwerpers' ),
		'menu_name'             => __( 'Agenda', 'bwhontwerpers' ),
		'name_admin_bar'        => __( 'Agenda', 'bwhontwerpers' ),
		'archives'              => __( 'Agenda arhief', 'bwhontwerpers' ),
		'attributes'            => __( 'Attributen', 'bwhontwerpers' ),
		'parent_item_colon'     => __( 'Parent Item:', 'bwhontwerpers' ),
		'all_items'             => __( 'Alle activiteiten', 'bwhontwerpers' ),
		'add_new_item'          => __( 'Nieuwe activiteit toevoegen', 'bwhontwerpers' ),
		'add_new'               => __( 'Nieuw', 'bwhontwerpers' ),
		'new_item'              => __( 'Nieuwe activiteit', 'bwhontwerpers' ),
		'edit_item'             => __( 'Bewerken', 'bwhontwerpers' ),
		'update_item'           => __( 'Update', 'bwhontwerpers' ),
		'view_item'             => __( 'Bekijken', 'bwhontwerpers' ),
		'view_items'            => __( 'Activiteiten bekijken', 'bwhontwerpers' ),
		'search_items'          => __( 'Zoeken', 'bwhontwerpers' ),
		'not_found'             => __( 'Niet gevonden', 'bwhontwerpers' ),
		'not_found_in_trash'    => __( 'Niet gevonden in prullenbak', 'bwhontwerpers' ),
		'featured_image'        => __( 'Uitgelichte afbeelding', 'bwhontwerpers' ),
		'set_featured_image'    => __( 'Uitgelichte afbeelding instellen', 'bwhontwerpers' ),
		'remove_featured_image' => __( 'Uitgelichte afbeelding verwijderen', 'bwhontwerpers' ),
		'use_featured_image'    => __( 'Als uitgelichte afbeelding gebruiken', 'bwhontwerpers' ),
		'insert_into_item'      => __( 'Toevoegen aan activiteit', 'bwhontwerpers' ),
		'uploaded_to_this_item' => __( 'Geupload naar activiteit', 'bwhontwerpers' ),
		'items_list'            => __( 'Activiteiten lijst', 'bwhontwerpers' ),
		'items_list_navigation' => __( 'Activiteiten lijst navigatie', 'bwhontwerpers' ),
		'filter_items_list'     => __( 'Filter', 'bwhontwerpers' ),
	);
	$args = array(
		'label'                 => __( 'Activiteit', 'bwhontwerpers' ),
		'description'           => __( 'Celebrating diversity agenda', 'bwhontwerpers' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'excerpt', ),
		'taxonomies'            => array( 'category' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-calendar-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'agenda', $args );
	
	$labels = array(
		'name'                  => _x( 'Projecten', 'Post Type General Name', 'bwhontwerpers' ),
		'singular_name'         => _x( 'Project', 'Post Type Singular Name', 'bwhontwerpers' ),
		'menu_name'             => __( 'Project', 'bwhontwerpers' ),
		'name_admin_bar'        => __( 'Project', 'bwhontwerpers' ),
		'archives'              => __( 'Project archief', 'bwhontwerpers' ),
		'attributes'            => __( 'Attributen', 'bwhontwerpers' ),
		'parent_item_colon'     => __( 'Parent Item:', 'bwhontwerpers' ),
		'all_items'             => __( 'Alle projecten', 'bwhontwerpers' ),
		'add_new_item'          => __( 'Nieuw project toevoegen', 'bwhontwerpers' ),
		'add_new'               => __( 'Nieuw', 'bwhontwerpers' ),
		'new_item'              => __( 'Nieuw project', 'bwhontwerpers' ),
		'edit_item'             => __( 'Bewerken', 'bwhontwerpers' ),
		'update_item'           => __( 'Update', 'bwhontwerpers' ),
		'view_item'             => __( 'Bekijken', 'bwhontwerpers' ),
		'view_items'            => __( 'Projecten bekijken', 'bwhontwerpers' ),
		'search_items'          => __( 'Zoeken', 'bwhontwerpers' ),
		'not_found'             => __( 'Niet gevonden', 'bwhontwerpers' ),
		'not_found_in_trash'    => __( 'Niet gevonden in prullenbak', 'bwhontwerpers' ),
		'featured_image'        => __( 'Uitgelichte afbeelding', 'bwhontwerpers' ),
		'set_featured_image'    => __( 'Uitgelichte afbeelding instellen', 'bwhontwerpers' ),
		'remove_featured_image' => __( 'Uitgelichte afbeelding verwijderen', 'bwhontwerpers' ),
		'use_featured_image'    => __( 'Als uitgelichte afbeelding gebruiken', 'bwhontwerpers' ),
		'insert_into_item'      => __( 'Toevoegen aan project', 'bwhontwerpers' ),
		'uploaded_to_this_item' => __( 'Geupload naar project', 'bwhontwerpers' ),
		'items_list'            => __( 'Projecten lijst', 'bwhontwerpers' ),
		'items_list_navigation' => __( 'Projecten lijst navigatie', 'bwhontwerpers' ),
		'filter_items_list'     => __( 'Filter', 'bwhontwerpers' ),
	);
	$args = array(
		'label'                 => __( 'Project', 'bwhontwerpers' ),
		'description'           => __( 'Celebrating diversity projecten', 'bwhontwerpers' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', ),
		'taxonomies'            => array( 'category' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-category',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'rewrite'				=> array( 'slug' => 'projecten', 'with_front' => false, 'feeds' => true, 'has_archive' => true ),
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'project', $args );

}
add_action( 'init', 'bwh_register_posttypes', 0 );

add_filter( "pre_get_posts", "bwh_alter_queries");
function bwh_alter_queries( $query ) {
	
	if (is_archive()) {
		if ( !is_admin() && $query->is_main_query() && in_array($query->get('post_type'), array('agenda') ) ) {
			if ( isset($query->query['filter']) ) {
				$tax_query = 
						array(
							'taxonomy'=>'category',
							'terms' => $query->query['filter'],
							'field' => 'slug',
						);
				$query->tax_query->queries[] = $tax_query;
				$query->query_vars['tax_query'] = $query->tax_query->queries;
			}
			
			$meta_query = 
				array(
					'key' => 'datum',
					'value' => date( 'Y-m-d' ),
					'compare' => '>=',
					'type' => 'DATE',
				);
			$query->meta_query->queries[] = $meta_query;
			$query->query_vars['meta_query'] = $query->meta_query->queries;
			
			
			$query->set("posts_per_page", 9999);
			$query->set("orderby","meta_value_num");
			$query->set("meta_key","datum");
			$query->set("order", "ASC");
			$query->set("no_found_rows", true);
			$query->set("update_post_term_cache", true);
			$query->set("update_post_meta_cache", false);
		}
	}
	
	if ( is_admin() && $query->is_main_query() && in_array($query->get('post_type'), array('agenda') ) ) {
		$query->set("orderby","meta_value_num");
		$query->set("meta_key","datum");
		$query->set("order", "ASC");
	}
    
}

Jigsaw::add_column('agenda', 'Datum', function($pid) {
	if ( get_field( 'datum', $pid) && !get_field( 'einddatum', $pid ) ) {
		echo date_i18n( 'd F Y', strtotime( get_field( 'datum', $pid ), false ) ); 
	} elseif ( get_field( 'datum', $pid) && get_field( 'einddatum', $pid ) ) {
		echo date_i18n( 'd F', strtotime( get_field( 'datum', $pid ), false ) ); 
		echo " - " . date_i18n( 'd F Y', strtotime( get_field( 'einddatum', $pid ), false ) );
	}	
}, 2);

Jigsaw::remove_column('agenda', 'date');


// Add image sizes
add_image_size( 'agenda-visual-hdpi', 810, 415, true );
add_image_size( 'agenda-visual-mdpi', 527, 270, true );
add_image_size( 'project-visual-hdpi', 1920, 1260, true );

add_image_size( 'agenda-poster-hdpi', 375, 487, true );
add_image_size( 'agenda-poster-mdpi', 250, 325, true );

//add_image_size( 'project-visual-mdpi', 527, 270, true );
add_image_size( 'exposition-visual-hdpi', 573, 786, true );
add_image_size( 'exposition-visual-mdpi', 382, 524, true );

?>

