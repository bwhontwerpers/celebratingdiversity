<?php
/* Loop item: agenda (uitgelicht) */
global $post;
?>

<a class="article article--agendauitgelicht" href="<?php the_permalink(); ?>">
	<div class="article__agenda__visual">
		<label><?php _e('uitgelicht','celebratingdiversity'); ?></label>
		<?php the_post_thumbnail( 'agenda-poster-mdpi' ); ?>
		<h2><?php the_title(); ?></h2>
		<?php if ( get_field( 'tickets') ) { ?>
			<a href="<?php the_field('tickets'); ?>" class="article__button article__button--tickets" target="_blank"><?php _e('Bestel kaarten', 'celebratingdiversity'); ?></a>
		<?php } ?>
		<?php if ( 0 < strlen(get_the_content() ) ) { ?>
			<a href="<?php the_permalink(); ?>" class="article__button article__button--more"><?php _e('Lees verder', 'celebratingdiversity'); ?></a>
		<?php } ?>
	</div>
</a>