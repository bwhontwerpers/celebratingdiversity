<?php
/* Header template for homepage */
?>

<button class="mobile__button visible-xs">&nbsp;</button>
<div id="headervisual">
	<div class="header__visual__wrapper">
		<?php echo wp_get_attachment_image( get_field( 'single-header', 'option' ), 'fullsize', false, array( 'class' => 'header__visual' ) ); ?>
	</div>
</div>

<div id="header" class="container-fluid">
	<div class="header__logo" style="width: 120px; margin-top: -30px; margin-left: 30px;">
		<a href="<?php echo get_home_url(); ?>" title="">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/cb_logo.svg" width="120" height="100" alt="Celebrating Diversity logo" />
		</a>
	</div>
	<div class="header__social">

		<a href="<?php the_field( 'social-twitter-url', 'option' ); ?>" target="_blank" title="<?php _e('Volg ons op Twitter','celebratingdiversity'); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/cb_icon_twitter.svg" alt="Twitter icon" /></a>
		<a href="<?php the_field( 'social-facebook-url', 'option' ); ?>" target="_blank" title="<?php _e('Volg ons op Facebook','celebratingdiversity'); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/cb_icon_facebook.svg" alt="Facebook icon" /></a>
		
		<ul class="header__languages">
			<?php
			$languages = icl_get_languages(); 
			foreach ( $languages as &$language ) {
				if ( 1 == $language['active'] ) { 
					?><label class="header__languages__current"><?php echo $language['native_name']; ?></label><?php
				}
			}
			?>
			<?php
			$languages = icl_get_languages(); 
			foreach ( $languages as &$language ) {
				if ( 1 == $language['active'] ) { 
					?><li class="current"><?php echo $language['native_name']; ?></li><?php
				} else { 
					?><li><a href="<?php echo $language['url']; ?>"><?php echo $language['native_name']; ?></a></li><?php
				}
			}
			?>
		</ul>
		
	</div>
	<div class="header__menu">
		<div class="col-xs-12">
			<?php wp_nav_menu( array( 'menu'=>'hoofdmenu', 'container'=>false, 'menu_class'=>'menu hidden-xs' ) ); ?>
		</div>
	</div>
</div>

<?php if ( is_singular('agenda') ) { ?>
	<button onclick="document.location='/agenda/';" class="button__back"></button>
<?php } elseif ( is_singular('post') ) { ?>
	<button onclick="document.location='/blog/';" class="button__back"></button>
<?php } elseif ( is_page() ) { if ( 0 == $post->post_parent ) { $parent = get_home_url(); } else { $parent = get_permalink($post->post_parent); } ?>
	<button onclick="document.location='<?php echo $parent; ?>';" class="button__back"></button>
<?php } elseif ( is_singular('project') ) { ?>
	<button onclick="document.location='/projecten/';" class="button__back"></button>
<?php } ?>

