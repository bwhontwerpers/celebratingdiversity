<?php
/* Loop item: blog (default) */
global $post;
?>

<a class="article article--blogdefault" href="<?php the_permalink(); ?>">
	<div class="article__meta">
		<?php _e('Nieuws','celebratingdiversity'); ?> &bull; <?php echo get_the_author_meta('display_name'); ?> &bull; <?php echo date_i18n( 'd F Y', strtotime( get_field( 'datum' ) ) ); ?>
	</div>
	<h3><?php the_title(); ?></h3>
	<?php the_excerpt(); ?>
	
	<div class="article__labels">
		<?php if ( get_field( 'locatie' ) ) { $location = get_field( 'locatie' ); ?><label>Locatie: <?php echo $location['address']; ?></label><?php } ?>
		<?php if ( get_field( 'entree' ) ) { ?><label>Entree: <?php the_field( 'entree' ); ?></label><?php } ?>
	</div>
	
	<div class="article__tags">
	<?php 
	$tags = wp_get_post_tags(get_the_ID());
	foreach ( $tags as $tag ) {
		?><span><?php echo $tag->name; ?></span><?php
	}
	?>
	</div>
	
	<div class="article__visual__wrapper">
		<?php the_post_thumbnail( 'agenda-visual-hdpi', array() ); ?>
	</div>
</a>