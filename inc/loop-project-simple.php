<?php
/* Loop item: project (simple) */
global $post;
?>

<a class="article article--projectsimple" href="<?php the_permalink(); ?>">
	<div class="article__visual__wrapper">
		<?php the_post_thumbnail( 'exposition-visual-hdpi', array() ); ?>
	</div>
	<h3><?php echo the_title(); ?></h3>
</a>