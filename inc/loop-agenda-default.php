<?php
/* Loop item: agenda (default) */
global $post;
?>

<div class="article article--agendadefault">
	<time datetime="<?php echo date_i18n( 'Y-m-d', strtotime( get_field( 'datum' ) ) ); ?>">
		<?php if ( get_field( 'einddatum' ) ) { ?>
			<small><?php echo date_i18n( 'd M', strtotime( get_field( 'datum' ) ) ); ?> - <?php echo date_i18n( 'd M Y', strtotime( get_field( 'einddatum' ) ) ); ?></small>
		<?php } else { ?>
			<?php echo date_i18n( 'd M Y', strtotime( get_field( 'datum' ) ) ); ?>
		<?php } ?>
	</time>
	<a href="<?php the_permalink(); ?>">
		<div class="article__visual__wrapper">
			<?php the_post_thumbnail( 'agenda-visual-hdpi', array() ); ?>
		</div>
		<div class="article__labels matchheight">
			<?php if ( get_field( 'locatie' ) ) { $location = get_field( 'locatie' ); ?><label><?php _e('Locatie','celebratingdiversity'); ?>: <?php echo $location['address']; ?></label><?php } ?>
			<?php if ( get_field( 'entree' ) ) { ?><label><?php _e('Entree','celebratingdiversity'); ?>: <?php the_field( 'entree' ); ?></label><?php } ?>
			<?php if ( get_field( 'tijd' ) ) { ?><label><?php _e('Tijd','celebratingdiversity'); ?>: &nbsp;&nbsp;&nbsp;&nbsp;<?php the_field( 'tijd' ); ?></label><?php } ?>
		</div>
		<h3><?php the_title(); ?></h3>
		<?php the_excerpt(); ?>
	</a>
	<?php if ( get_field( 'tickets') ) { ?>
		<a href="<?php the_field('tickets'); ?>" class="article__button article__button--tickets" target="_blank"><?php _e('Bestel kaarten', 'celebratingdiversity'); ?></a>
	<?php } ?>
	<?php if ( 0 < strlen(get_the_content() ) ) { ?>
		<a href="<?php the_permalink(); ?>" class="article__button article__button--more"><?php _e('Lees verder', 'celebratingdiversity'); ?></a>
	<?php } ?>
</div>