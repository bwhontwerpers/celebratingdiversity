<?php
/* Header template for homepage */
?>

<button class="mobile__button visible-xs">&nbsp;</button>
<div id="headervisual">
	<div class="header__visual__wrapper">
		<?php echo wp_get_attachment_image( get_field( 'header_visual' ), 'fullsize', false, array( 'class' => 'header__visual' ) ); ?>
	</div>
</div>
	
<div id="header" class="container-fluid">
	<a href="<?php echo get_home_url(); ?>" class="header__logo">
		<h1>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/cb_logo.svg" width="193" height="144" alt="Celebrating Diversity logo" />
		</h1>
	</a>
	<div class="header__social">
		<a href="<?php the_field( 'social-twitter-url', 'option' ); ?>" target="_blank" title="<?php _e('Volg ons op Twitter','celebratingdiversity'); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/cb_icon_twitter.svg" alt="Twitter icon" /></a>
		<a href="<?php the_field( 'social-facebook-url', 'option' ); ?>" target="_blank" title="<?php _e('Volg ons op Facebook','celebratingdiversity'); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/cb_icon_facebook.svg" alt="Facebook icon" /></a>

		<ul class="header__languages">
			<?php
			$languages = icl_get_languages(); 
			foreach ( $languages as &$language ) {
				if ( 1 == $language['active'] ) { 
					?><label class="header__languages__current"><?php echo $language['native_name']; ?></label><?php
				}
			}
			?>
			<?php
			$languages = icl_get_languages(); 
			foreach ( $languages as &$language ) {
				if ( 1 == $language['active'] ) { 
					?><li class="current"><?php echo $language['native_name']; ?></li><?php
				} else { 
					?><li><a href="<?php echo $language['url']; ?>"><?php echo $language['native_name']; ?></a></li><?php
				}
			}
			?>
		</ul>
	</div>
	<div class="header__tagline">
		<h2><?php the_field( 'home-header-title', 'option' ); ?></h2>
		<h3>Fries landschap laat van zich horen!</h3>
		<button onclick="document.location='/programma2018/agenda/';" class="home_btn"><?php the_field( 'home-header-button', 'option' ); ?></button>
	</div>
	<div class="header__menu">
		<div class="col-sm-12 col-md-12">
			<?php wp_nav_menu( array( 'menu'=>'hoofdmenu', 'container'=>false, 'menu_class'=>'menu hidden-xs' ) ); ?>
		</div>
	</div>
</div>
