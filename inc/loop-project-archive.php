<?php
/* Loop item: agenda (default) */
global $post;
?>

<a class="article article--archiveproject" href="<?php the_permalink(); ?>">
	<div class="article__visual__wrapper">
		<?php the_post_thumbnail( 'exposition-visual-hdpi', array() ); ?>
	</div>
	<h3><?php echo the_title(); ?></h3>
</a>