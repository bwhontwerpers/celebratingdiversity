<?php
/* Loop item: agenda (archive) */
global $post;
if ( has_term( icl_object_id( 3, 'category', TRUE ), 'category' ) ) {
	$class = "static";
} else { 
	$class = false;
}
?>

<div class="article article--agendaarchive <?php echo $class; ?>">
	<div class="article__agenda__visual">
		<?php the_post_thumbnail( 'agenda-poster-mdpi' ); ?>
		<time datetime="<?php echo date_i18n( 'Y-m-d', strtotime( get_field( 'datum' ) ) ); ?>">
			<span class="day"><?php echo date_i18n( 'l', strtotime( get_field( 'datum' ) ) ); ?></span>
			<span class="date"><?php echo date_i18n( 'd M', strtotime( get_field( 'datum' ) ) ); ?></span>
			<?php if ( get_field('einddatum') ) { ?>
			<span class="date date--end"><?php echo date_i18n( 'd M', strtotime( get_field( 'einddatum' ) ) ); ?></span>
			<span class="year"><?php echo date_i18n( 'Y', strtotime( get_field( 'datum' ) ) ); ?></span>
			<?php } else { ?>
			<span class="year"><?php echo date_i18n( 'Y', strtotime( get_field( 'datum' ) ) ); ?></span>
			<?php } ?>
		</time>
	</div>
	<div class="article__agenda__details">
		<a href="<?php the_permalink(); ?>">
		<div class="article__labels">
			<?php if ( get_field( 'locatie' ) ) { $location = get_field( 'locatie' ); ?><label><?php _e('Locatie','celebratingdiversity'); ?>: &nbsp;&nbsp;<?php echo $location['address']; ?></label><?php } ?>
			<?php if ( get_field( 'entree' ) ) { ?><label><?php _e('Entree','celebratingdiversity'); ?>: &nbsp;&nbsp;&nbsp;&nbsp;<?php the_field( 'entree' ); ?></label><?php } ?>
			<?php if ( get_field( 'tijd' ) ) { ?><label><?php _e('Tijd','celebratingdiversity'); ?>: &nbsp;&nbsp;&nbsp;&nbsp;<?php the_field( 'tijd' ); ?></label><?php } ?>
		</div>
		<h2><?php the_title(); ?></h2>
		<?php the_excerpt(); ?>
		</a>
		<?php if ( get_field( 'tickets') ) { ?>
			<a href="<?php the_field('tickets'); ?>" class="article__button article__button--tickets" target="_blank"><?php _e('Bestel kaarten', 'celebratingdiversity'); ?></a>
		<?php } ?>
		<?php if ( 0 < strlen(get_the_content() ) ) { ?>
			<a href="<?php the_permalink(); ?>" class="article__button article__button--more"><?php _e('Lees verder', 'celebratingdiversity'); ?></a>
		<?php } ?>
	</div>
</div>