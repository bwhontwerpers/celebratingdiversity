<?php
/* Loop item: agenda (default) */
global $post;
?>

<a class="article article--agendasimple agenda-matchheight" href="<?php the_permalink(); ?>">
	<time datetime="<?php echo date_i18n( 'Y-m-d', strtotime( get_field( 'datum' ) ) ); ?>">
		<?php if ( get_field( 'einddatum' ) ) { ?>
			<small><?php echo date_i18n( 'd M', strtotime( get_field( 'datum' ) ) ); ?> - <?php echo date_i18n( 'd M Y', strtotime( get_field( 'einddatum' ) ) ); ?></small>
		<?php } else { ?>
			<?php echo date_i18n( 'd M Y', strtotime( get_field( 'datum' ) ) ); ?>
		<?php } ?>
	</time>
	<div class="article__labels matchheight">
		<?php if ( get_field( 'locatie' ) ) { $location = get_field( 'locatie' ); ?><label><?php _e('Locatie','celebratingdiversity'); ?>: <?php echo $location['address']; ?></label><?php } ?>
		<?php if ( get_field( 'entree' ) ) { ?><label><?php _e('Entree','celebratingdiversity'); ?>: <?php the_field( 'entree' ); ?></label><?php } ?>
		<?php if ( get_field( 'tijd' ) ) { ?><label><?php _e('Tijd','celebratingdiversity'); ?>: &nbsp;&nbsp;&nbsp;&nbsp;<?php the_field( 'tijd' ); ?></label><?php } ?>
	</div>
	<h3><?php the_title(); ?></h3>
	<?php the_excerpt(); ?>
</a>