<?php
/* Loop item: agenda (default) */
global $post;
?>

<a class="article article--agendaproject" href="<?php the_permalink(); ?>">
	<time datetime="<?php echo date_i18n( 'Y-m-d', strtotime( get_field( 'datum' ) ) ); ?>"><?php echo date_i18n( 'd M Y', strtotime( get_field( 'datum' ) ) ); ?></time>
	<div class="article__visual__wrapper">
		<?php the_post_thumbnail( 'exposition-visual-hdpi', array() ); ?>
	</div>
	<h3><?php echo the_title(); ?></h3>
</a>