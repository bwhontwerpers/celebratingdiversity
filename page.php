<?php
/* 
Page template for pages
*/
get_header();
?>

<?php while (have_posts() ) { the_post(); if ( 0 == $post->post_parent ) { $parent = get_the_ID(); } else { $parent = $post->post_parent; } ?>

<div id="frame" class="container">
	<div class="row">
		<div class="wrapper">
			
			<div class="col-md-8 col-md-offset-2">
				<div class="content">
					<h1 class="header"><?php the_title(); ?></h1>
					
					<?php
					$subpages = get_posts( array( 'post_type' => 'page', 'post_parent' => $parent ) );
					if ( 0 < count($subpages) ) {
						?><div class="content__subpages"><?php
						foreach ( $subpages as &$subpage ) {
							if ( $subpage->ID == get_the_ID() ) { $class="current-menu-item"; } else { $class=""; }
							?>
							<a class="subpage__button <?php echo $class; ?>" title="<?php echo $subpage->post_title; ?>" href="<?php echo get_permalink($subpage->ID); ?>"><?php echo $subpage->post_title; ?></a>
							<?php
						}
						?></div><?php
					}
					?>
					<?php if ( has_post_thumbnail() ) { the_post_thumbnail( 'agenda-visual-hdpi', array( 'class' => 'inline__visual' ) ); } ?>
					<?php the_content(); ?>
					<div class="article__share">
						<a href="https://twitter.com/home?status=<?php the_permalink(); ?>" target="_blank" class="share__twitter" title="Delen via Twitter">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/cb_icon_twitter.svg" alt="Twitter icon" />
						</a>
						<a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank" class="share__facebook" title="Delen via Facebook">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/cb_icon_facebook.svg" alt="Facebook icon" />
						</a>
					</div>
				</div>
			</div>
						
		</div>
		
	</div>
</div>

<?php } ?>

<?php
get_footer();
?>