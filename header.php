<!DOCTYPE html>
<!--[if lt IE 9]>
<html class="ie8"><![endif]-->
<!--[if IE 9]>
<html class="ie"><![endif]-->
<!--[if gt IE 8]><!-->
<html <?php language_attributes(); ?>> <!--<![endif]-->

  <head>
    
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title><?php bloginfo('name'); ?> <?php wp_title('&raquo;', true, 'left'); ?></title>

    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <base href="/">

    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <meta name ="viewport" content = "initial-scale=1, user-scalable=no">
    <meta name="resource-type" content="document" />
    <meta http-equiv="content-type" content="text/html; charset=US-ASCII" />
    <meta http-equiv="content-language" content="nl-NL" />
    <meta name="author" content="BW H Ontwerpers" />
    <meta name="contact" content="andries@bwhontwerpers.nl" />
    <meta name="copyright" content="Copyright (c)2016 BW H Ontwerpers. All Rights Reserved." />
    <meta name="description" content="<?php if ( is_single() ) { single_post_title('', true); } else { bloginfo('description'); } ?>" />
    <meta name="keywords" content="" />
    
    <!-- Opengraph -->
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<?php bloginfo('name'); ?><?php wp_title('&raquo;', true, 'left'); ?>" />
    <meta property="og:url" content="<?php the_permalink(); ?>" />
    <meta property="og:description" content="<?php if ( is_single() ) { the_content(); } else { bloginfo('description'); } ?>" />
    <meta property="og:site_name" content="<?php bloginfo('name'); ?>" />
    
    <?php if ( has_post_thumbnail() ) { ?>
    <meta property="og:image" content="<?php echo the_post_thumbnail_url('large');?>" />
		<?php } else { ?>
    <meta property="og:image" content="<?php header_image(); ?>" />
    <?php } ?> 
  	
  	<!-- Favicon -->
	<link rel="apple-touch-icon" sizes="180x180" href="/wp-content/themes/celebratingdiversity/dist/img/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="/wp-content/themes/celebratingdiversity/dist/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="/wp-content/themes/celebratingdiversity/dist/img/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="/wp-content/themes/celebratingdiversity/dist/img/favicon/manifest.json">
	<link rel="mask-icon" href="/wp-content/themes/celebratingdiversity/dist/img/favicon/safari-pinned-tab.svg" color="#7e9a86">
	<meta name="theme-color" content="#7e9a86">
  	
    <!-- Wordpress header scripts and styles -->
    <?php wp_head(); ?>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

	<script> (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-56931585-2', 'auto'); ga('send', 'pageview');
	</script>

    </head>
  
    <body <?php body_class(); ?>>
	 
	<div class="mobile__canvas ani hidden-sm">
		<div class="header__social">
			<?php
			$languages = icl_get_languages(); 
			foreach ( $languages as &$language ) {
				if ( 1 == $language['active'] ) { 
					$lang_active = "<strong>{$language['native_name']}</strong>"; 
				} else { 
					$link = $language['url'];
					$lang_inactive = $language['native_name']; 
				}
			}
			?>
			<a href="<?php echo $link; ?>" class="lang"><?= $lang_active."".$lang_inactive; ?></a>
			<a href="<?php the_field( 'social-twitter-url', 'option' ); ?>" target="_blank" title="<?php _e('Volg ons op Twitter','celebratingdiversity'); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/cb_icon_twitter.svg" alt="Twitter icon" /></a>
			<a href="<?php the_field( 'social-facebook-url', 'option' ); ?>" target="_blank" title="<?php _e('Volg ons op Facebook','celebratingdiversity'); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/cb_icon_facebook.svg" alt="Facebook icon" /></a>
		</div>
		
		<div class="mobile__menu__wrapper">
			<?php wp_nav_menu( array( 'menu'=>'mobielmenu', 'container'=>false, 'menu_class'=>'menu hidden-sm' ) ); ?>
		</div>
	</div>
	
    <?php
	if ( is_front_page() ) { get_template_part( 'inc/header', 'start' ); } else {
	get_template_part( 'inc/header', 'post' ); }
	?>
  