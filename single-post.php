<?php
/* 
Page template for single blog items
*/
get_header();
?>

<?php while (have_posts() ) { the_post(); ?>

<div id="frame" class="container">
	<div class="row">
		<div class="wrapper">
			
			<div class="col-md-8 col-md-offset-2">
				<div class="content">
					<h1 class="header"><?php the_title(); ?></h1>
					<div class="article__meta">
						<?php _e('Blog','celebratingdiversity'); ?> &bull; <?php echo get_the_author_meta('display_name'); ?> &bull; <?php echo date_i18n( 'd F Y', strtotime( get_field( 'datum' ) ) ); ?>
					</div>
					<?php if ( has_post_thumbnail() ) { the_post_thumbnail( 'agenda-visual-hdpi', array( 'class' => 'inline__visual' ) ); } ?>
					<?php the_content(); ?>
					<div class="article__share">
						<a href="https://twitter.com/home?status=<?php the_permalink(); ?>" target="_blank" class="share__twitter" title="Delen via Twitter">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/cb_icon_twitter.svg" alt="Twitter icon" />
						</a>
						<a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank" class="share__facebook" title="Delen via Facebook">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/cb_icon_facebook.svg" alt="Facebook icon" />
						</a>
					</div>
				</div>
			</div>
						
		</div>
		
	</div>
</div>

<?php } ?>

<?php
get_footer();
?>