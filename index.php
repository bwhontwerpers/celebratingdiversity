<?php
/**
 *
 * @package WordPress
 * @subpackage BWH
 */
 
?>

<?php if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') { $ajax = true; } else { $ajax = false; } ?>

<?php if(!$ajax) { ?>

  <?php get_header(); ?>

<?php } ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

  <h1><?php the_title(); ?></h1>
      
  <?php the_content(); ?>    
    
  <?php endwhile; else : ?>
      
    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
      
  <?php endif; ?>

<?php if(!$ajax) { ?>
  
  <?php get_footer(); ?>

<?php } ?>