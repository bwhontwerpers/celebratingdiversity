// Gruntfile.js

// our wrapper function (required by grunt and its plugins)
// all configuration goes inside this function
module.exports = function(grunt) {

  // ===========================================================================
  // CONFIGURE GRUNT ===========================================================
  // ===========================================================================
  
  grunt.initConfig({

    // get the configuration info from package.json ----------------------------
    // this way we can use things like name and version (pkg.name)
    pkg: grunt.file.readJSON('package.json'),

	// all of our configuration will go here
	
	  // configure jshint to validate js files -----------------------------------
	  jshint: {
      options: {
        reporter: require('jshint-stylish') // use jshint-stylish to make our errors look and read good
      },

	  // when this task is run, lint the Gruntfile and all js files in src
      build: ['Grunfile.js', 'src/**/*.js']
    },
    
    concat: {
      options: {
        // define a string to put between each file in the concatenated output
        separator: ';'
      },
      dist: {
        // the files to concatenate
        src: ['src/js/*.js'],
        // the location of the resulting JS file
        dest: 'dist/js/<%= pkg.name %>.js'
      }
    },
    
    // configure uglify to minify js files -------------------------------------
    uglify: {
      options: {
        banner: '/*\n <%= pkg.name %> - <%= pkg.author %>, <%= grunt.template.today("yyyy-mm-dd") %> \n*/\n',
        mangle: false
      },
      build: {
        files: {
          'dist/js/<%= pkg.name %>.min.js': ['<%= concat.dist.dest %>']
        }
      }
    },
    
    // compile less stylesheets to css -----------------------------------------
    less: {
      build: {
        options: {
          strictMath: true,
          compress: true,
          yuicompress: true,
          optimization: 2,
          banner: '/*\n <%= pkg.name %> - <%= pkg.author %>, <%= grunt.template.today("yyyy-mm-dd") %> \n*/\n'
        },
        files: {
          'dist/css/screen.min.css': 'src/less/screen.less'
        }
      },
    },
    
    // configure watch to auto update ------------------------------------------
    watch: {
      stylesheets: {
        files: ['src/less/*.less'],
        tasks: ['less']
      },
      scripts: {
        files: 'src/js/*.js',
        tasks: ['jshint', 'concat', 'uglify']
      }
    }

  });
  
  // ===========================================================================
  // CREATE TASKS ==============================================================
  // ===========================================================================
  
  grunt.registerTask('default', ['jshint', 'concat', 'uglify', 'less']);

  // ===========================================================================
  // LOAD GRUNT PLUGINS ========================================================
  // ===========================================================================
  // we can only load these if they are in our package.json
  // make sure you have run npm install so our app can find these
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-copy');

};