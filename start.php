<?php
/* 
Page template for homepage
Template name: Startpagina template
*/
get_header();
global $post;
$exclude_events = array();
?>

<div id="frame" class="container">
	<div class="row">

		<div class="wrapper">
			
			<div class="col-xs-12 page-intro">
				<article>
					<?php the_field( 'home-intro-text' ); ?>
				</article>
			</div>
						
			<div class="col-md-12 col-xs-12">
				<h2 class="header header--margin"><?php _e('Programma','celebratingdiversity'); ?></h2>
			</div>
			
			<div class="col-md-7 col-sm-7 col-xs-12">
			<?php
			/* Upcoming events: highlighted */
			//$events = get_posts( array( 'post_type' => 'agenda', 'posts_per_page' => 1, 'orderby' => 'meta_value_num', 'meta_key' => 'datum', 'order' => 'ASC', 'suppress_filters' => 0 ) );
			$events = get_field('highlighted-expositions');
			if (isset($events[0])) {
				$post = $events[0];
				setup_postdata( $post );
				array_push( $exclude_events, get_the_ID() );
				?><?php get_template_part( 'inc/loop', 'agenda-default' ); ?><?php
				wp_reset_postdata();
			}
			?>
			</div>
	
			<div class="col-md-5 col-sm-5 col-xs-12">
				<?php
				/* Upcoming fixed exhibition */
				
				if (isset($events[1])) {
					$post = $events[1];
					setup_postdata( $post );
					array_push( $exclude_events, get_the_ID() );
					get_template_part( 'inc/loop', 'agenda-project' );
					wp_reset_postdata();					
				}
				?>
			</div>

			<div class="agenda__items__homepage">			
			<?php
			/* Upcoming events: divided over 4 columns */
			$events = get_posts( 
				array( 
					'post_type' => 'agenda', 
					'posts_per_page' => 4, 
					'orderby' => 'meta_value_num', 
					'meta_key' => 'datum', 
					'order' => 'ASC', 
					'exclude' => $exclude_events, 
					'suppress_filters' => 0,
					'meta_query' => array(
						array(
							'key' => 'datum',
							'value' => date('Y-m-d'),
							'compare' => '>=',
							'type' => 'DATE',
						)
					),
				) 
			);
			foreach ( $events as $post ) {
				setup_postdata( $post );
				?><div class="col-lg-3 col-md-6 col-sm-6 col-xs-12"><?php get_template_part( 'inc/loop', 'agenda-simple' ); ?></div><?php
			}
			wp_reset_postdata();
			?>			
			</div>
			
			<div class="col-md-12 col-xs-12">
				<p>&nbsp;</p>
				<button onclick="document.location='<?php echo get_post_type_archive_link( 'agenda' ); ?>';" class="button button--purple button--arrow button--center"><?php _e('Volledig programma','celebratingdiversity'); ?></button>
				<p>&nbsp;</p>
			</div>
					
			<div class="col-md-12 col-xs-12">
				<h2 class="header"><?php _e('Nieuws','celebratingdiversity'); ?></h2>
			</div>
			
			<div class="blog__items__homepage">			
			<?php
			/* Latest blog items: divided over 2 columns */
			$articles = get_posts( array( 'post_type' => 'post', 'posts_per_page' => 2, 'orderby' => 'date', 'order' => 'DESC', 'category_name' => 'blog', 'suppress_filters' => 0 ) );
			foreach ( $articles as $post ) {
				setup_postdata( $post );
				?><div class="col-md-6 col-sm-6 col-xs-12"><?php get_template_part( 'inc/loop', 'blog-default' ); ?></div><?php
			}
			wp_reset_postdata();
			?>			
			</div>
			
			<div class="col-md-12 col-xs-12">
				<p>&nbsp;</p>
				<button onclick="document.location='<?php // echo get_term_link( 'blog' ); ?>/blog/';" class="button button--purple button--arrow button--center"><?php _e('Meer nieuws','celebratingdiversity'); ?></button>
				<p>&nbsp;</p>
			</div>
			
			<div class="projecten__items__homepage">

				<div class="col-md-12 col-xs-12">
					<h2 class="header"><?php _e('Projecten','celebratingdiversity'); ?></h2>
				</div>

			<?php
			/* Projects: divided over 4 columns */
			$projects = get_posts( array( 'post_type' => 'project', 'posts_per_page' => -1, 'orderby' => 'date', 'order' => 'DESC', 'suppress_filters' => 0 ) );
			foreach ( $projects as $post ) {
				setup_postdata( $post );
				?><div class="col-md-3 col-sm-6 col-xs-12"><?php get_template_part( 'inc/loop', 'project-simple' ); ?></div><?php
			}
			wp_reset_postdata();
			?>
			</div>
			
			<div class="col-md-12 col-xs-12">
				<p>&nbsp;</p>
				<button onclick="document.location='<?php echo get_post_type_archive_link( 'project' ); ?>';" class="button button--purple button--arrow button--center"><?php _e('Alle projecten','celebratingdiversity'); ?></button>
				<p>&nbsp;</p>
			</div>
			
			<div class="content__homepage col-xs-12">
				
				<div class="col-md-6 col-xs-12">
					<h3><?php the_field( 'home-left-title' ); ?></h3>
					<?php the_field( 'home-left-content' ); ?>
					<p>&nbsp;</p>
					<a class="button button--purple button--arrow" href="<?php the_field( 'home-left-url' ); ?>"><?php _e( 'Lees meer', 'celebratingdiversity' ); ?></a>
				</div>
				
				<div class="col-md-6 col-xs-12">
					<h3><?php the_field( 'home-right-title' ); ?></h3>
					<?php the_field( 'home-right-content' ); ?>
					<p>&nbsp;</p>
					<p>
						<a href="<?php echo get_permalink(icl_object_id(36,'page')); ?>" class="button button--purple button--arrow"><?php _e( 'Zelf de biodiversiteit helpen', 'celebratingdiversity' ); ?></a>
						<a target="_blank" href="<?php the_field( 'social-twitter-url', 'option' ); ?>" class="button button--green button--arrow"><?php _e( 'Volg ons op Twitter', 'celebratingdiversity' ); ?></a>
						<a target="_blank" href="<?php the_field( 'social-facebook-url', 'option' ); ?>" class="button button--green button--arrow"><?php _e( 'Volg ons op Facebook', 'celebratingdiversity' ); ?></a>
					</p>
				</div>				
				
			</div>
			
		</div>

	</div>
</div>
			

<?php
get_footer();
?>