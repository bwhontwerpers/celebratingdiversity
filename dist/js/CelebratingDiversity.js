function bwh_matchheight_init() {
	'use strict';
	
	jQuery('.matchheight').matchHeight(
		{
			byRow: false,
			property: 'height',
			target: null,
			remove: false
		}
	);
	
	jQuery('.agenda-matchheight').matchHeight(
		{
			byRow: false,
			property: 'height',
			target: null,
			remove: false
		}
	);
	
}

function bwh_canvas_menu_init() {
	'use strict';
	
	jQuery('.mobile__button').on( 'click', function() { 
		jQuery('body').toggleClass('canvas--visible');
	});
}

function bwh_agenda_filter_init() {
	'use strict';
	
	jQuery("select[name='filter']").on('change', function() {
		if ( "reset" != jQuery(this).val() ) {
			document.location = jQuery(this).attr('data-archive')+"filter/"+jQuery(this).val()+"/";
		} else {
			document.location = jQuery(this).attr('data-archive')+"/";
		}
	});
	
	jQuery("input[name='showstatic']").on('change', function() {
		if ( jQuery('.article--agendaarchive.static:visible').length ) {
			jQuery('.article--agendaarchive.static').hide();
		} else {
			jQuery('.article--agendaarchive.static').show();
		}
	});
	
}

function bwh_language_init() {
	'use strict';
	
	jQuery(".header__languages label").on('click', function() {
		if ( jQuery('.header__languages li:visible').length ) {
			jQuery('.header__languages li').hide();
		} else {
			jQuery('.header__languages li').show();
		}
	});
	
}

function bwh_waypoints_init() {
	'use strict';
	var wpOffset = 0;
	
	if ( jQuery('body').hasClass('home') ) {
		wpOffset = "317px";
	} else {
		wpOffset = "250px";
	}
	
	var waypoints = jQuery('#frame').waypoint(function(direction) {
		if ( 'up' == direction ) {
			jQuery('body').removeClass('logo--hidden');
		} else {
			jQuery('body').addClass('logo--hidden');
		}
	}, {
		offset: wpOffset
	});
}

jQuery(window).load(function() {
  
	bwh_matchheight_init();
	bwh_canvas_menu_init();
	bwh_agenda_filter_init();
	bwh_language_init();
	bwh_waypoints_init();
	
});
