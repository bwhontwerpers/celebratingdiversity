<?php
/* 
Archive template for 'project' items
*/
get_header();
?>

<div id="frame" class="container">
	<div class="row">
		<div class="wrapper">
			
		<?php while (have_posts() ) { the_post(); ?>
			<div class="col-sm-6 col-xs-12"><?php get_template_part( 'inc/loop', 'project-archive' ); ?></div>
		<?php } ?>
						
		</div>
		
	</div>
</div>



<?php
get_footer();
?>