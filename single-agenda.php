<?php
/* 
Page template for single agenda items
*/
echo "testing";
get_header();
?>

<?php while (have_posts() ) { the_post(); ?>

<div id="frame" class="container">
	<div class="row">
		
		<div class="agenda__single__top">
			<div class="wrapper">
				<div class="col-md-8 col-md-offset-2">
					<div class="content">
						<time datetime="<?php echo date_i18n( 'Y-m-d', strtotime( get_field( 'datum' ) ) ); ?>">
							<span class="day"><?php echo date_i18n( 'l', strtotime( get_field( 'datum' ) ) ); ?></span>							
							<?php if ( get_field('einddatum') ) { ?>
							<span class="date"><?php echo date_i18n( 'd M', strtotime( get_field( 'datum' ) ) ); ?></span>
							<span class="date date--end"><?php echo date_i18n( 'd M', strtotime( get_field( 'einddatum' ) ) ); ?></span>
							<span class="year"><?php echo date_i18n( 'Y', strtotime( get_field( 'datum' ) ) ); ?></span>
							<?php } else { ?>
							<span class="date"><?php echo date_i18n( 'd M Y', strtotime( get_field( 'datum' ) ) ); ?></span>
							<?php } ?>
						</time>
						<h1 class="header"><?php the_title(); ?></h1>
						<div class="single__article__labels">
							<?php if ( get_field( 'locatie' ) ) { $location = get_field( 'locatie' ); ?><label><?php _e('Locatie','celebratingdiversity'); ?>: <?php echo $location['address']; ?></label><?php } ?>
							<?php if ( get_field( 'entree' ) ) { ?><label><?php _e('Entree','celebratingdiversity'); ?>: <?php the_field( 'entree' ); ?></label><?php } ?>
							<?php if ( get_field( 'tijd' ) ) { ?><label><?php _e('Tijd','celebratingdiversity'); ?>: <?php the_field( 'tijd' ); ?></label><?php } ?>
						</div>
						<?php if ( get_field( 'tickets') ) { ?>
						<div class="tickets__wrapper">
							<a href="<?php the_field('tickets'); ?>" class="article__button article__button--tickets" target="_blank"><?php _e('Bestel kaarten', 'celebratingdiversity'); ?></a>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
		
		<div class="wrapper">
			<div class="col-md-8 col-md-offset-2">
				<div class="content">
					<?php if ( has_post_thumbnail() ) { the_post_thumbnail( 'agenda-visual-hdpi', array( 'class' => 'inline__visual' ) ); } ?>
					<?php the_content(); ?>
					<div class="article__share">
						<a href="https://twitter.com/home?status=<?php the_permalink(); ?>" target="_blank" class="share__twitter" title="Delen via Twitter">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/cb_icon_twitter.svg" alt="Twitter icon" />
						</a>
						<a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank" class="share__facebook" title="Delen via Facebook">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/cb_icon_facebook.svg" alt="Facebook icon" />
						</a>
					</div>
				</div>
			</div>
		</div>
		
	<?php 
	if ( get_field( 'linked_category' ) ) { 
		$related = get_posts(
			array(
				'post_type' => 'agenda',
				'posts_per_page' => 4,
				'orderby' => 'meta_value_num',
				'meta_key' => 'datum',
				'order' => 'ASC',
				'cat' => get_field( 'linked_category' ),
			)
		);
		
		if ( 0 < count( $related ) ) {
		?>
		<div class="agenda__single__bottom">
			<div class="wrapper">
				<div class="col-xs-12">
					<h3><?php _e('Gerelateerde activiteiten','celebratingdiversity'); ?></h3>
				</div>
				
				<?php				
				foreach ( $related as $post ) {
					setup_postdata($post);
					?><div class="col-12 col-sm-6 col-lg-3"><?php
					get_template_part( 'inc/loop', 'agenda-simple');
					?></div><?php
					
				}
				wp_reset_postdata();
				?>
				</div>
			</div>
		</div>
		<?php 
		}
	} 
	?>
		
	</div>
</div>

<?php } ?>

<script type="text/javascript">
	document.getElementById("menu-item-70").className += " current-menu-item";
</script>

<?php
get_footer();
?>