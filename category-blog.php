<?php
/* 
Archive template for 'post' items
*/
get_header();
?>

<div id="frame" class="container">
	<div class="row">
		<div class="wrapper">
		
		<?php while (have_posts() ) { the_post(); ?>
			<div class="col-md-6 col-sm-6 col-xs-12 matchheight"><?php get_template_part( 'inc/loop', 'blog-default' ); ?></div>
		<?php } ?>
		
		</div>
		
	</div>
</div>



<?php
get_footer();
?>