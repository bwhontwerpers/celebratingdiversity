	
	<div id="footer">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-4 col-md-3 col-md-offset-1">
					<div class="form__text"><?php the_field( 'footer-newsletter-text', 'option' ); ?></div>
				</div>
				<div class="col-xs-12 col-sm-8 col-md-6">
					<div class="form__wrapper">
					<?php gravity_form( 1, false, false, false, null, true, 100, true ); ?>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-11 col-md-offset-1">
					<div class="footer__credits">
						<a href="http://2018.nl/nl/projecten#celebrating-diversity" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/dist/img/cb_kh2018_logo.svg" class="partner__logo partner__logo--alt" alt="Logo" /></a>
					</div>
				</div>
			</div>
		</div>
	</div>
	
    <?php wp_footer(); ?>
    
  </body>
  
</html>

