<?php
/* 
Archive template for 'agenda' items
*/
get_header();
?>

<div id="frame" class="container">
	<div class="row">
		<div class="wrapper">
			
			<div class="col-xs-12">
				<div class="filter__wrapper">
					<select name="filter" name="filter" data-archive="<?php echo get_post_type_archive_link( 'agenda' ); ?>">
						<?php if ( !get_query_var('filter') ) { ?>
						<option placeholder default disabled selected><?php _e('Filter op project', 'celebratingdiversity'); ?></option>
						<?php } else { ?>
						<option placeholder default value="reset"><?php _e('Alle projecten', 'celebratingdiversity'); ?></option>
						<?php } ?>

						<?php
						$projects = get_terms( array('taxonomy' => 'category', 'child_of' => 4, 'hide_empty' => true) );
						foreach ( $projects as $project ) {
							if ( $project->slug == get_query_var('filter') ) {
							?><option selected value="<?= $project->term_id; ?>"><?= $project->name; ?></option><?php	
							} else {
							?><option value="<?= $project->slug; ?>"><?= $project->name; ?></option><?php		
							}
						}
						?>
					</select>
					<input type="checkbox" name="showstatic" id="showstatic" value="1" checked="checked" /><label for="showstatic"><?php _e('Vaste exposities weergeven', 'celebratingdiversity' ); ?></label>
				</div>
			</div>

			
			<div class="col-sm-9 col-xs-12">
				<div class="">
					
					<?php while (have_posts() ) { the_post(); ?>
						<?php get_template_part( 'inc/loop', 'agenda-archive' ); ?>
					<?php } ?>

				</div>
			</div>

			<div class="col-md-3 col-xs-12">
				<?php
				$uitgelichtItems = get_posts( array( 'post_type' => 'agenda', 'posts_per_page' => 9999, 'cat' => array(icl_object_id(2,'category',TRUE) ) ) );
				if ( 0 < count($uitgelichtItems) ) {
					foreach ( $uitgelichtItems as &$post ) {
						setup_postdata( $post );
						get_template_part( 'inc/loop', 'agenda-uitgelicht' );
					}
				}
				wp_reset_postdata();
				?>
			</div>
						
		</div>
		
	</div>
</div>



<?php
get_footer();
?>