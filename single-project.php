<?php
/* 
Page template for single agenda items
*/
get_header();
?>

<?php while (have_posts() ) { the_post(); ?>

<div id="frame" class="container">
	<div class="row">
		
		<?php if ( get_field( 'project-header' ) ) { ?>
			<div class="project__content__header__wrapper">
				<h1 class="single__header__title"><?php the_title(); ?></h1>
				<?php echo wp_get_attachment_image( get_field( 'project-header' ), 'project-visual-hdpi', false, array( 'class' => 'project__content__header' ) ); ?>
			</div>
		<?php } ?>
		
		<div class="wrapper__project">
			<div class="wrapper wrapper--project">
			
				<div class="col-sm-7">
					<div class="content">
						<?php the_content(); ?>
						<div class="article__share">
							<a href="https://twitter.com/home?status=<?php the_permalink(); ?>" target="_blank" class="share__twitter" title="<?php _e('Delen via Twitter','celebratingdiversity'); ?>">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/cb_icon_twitter.svg" alt="Twitter icon" />
							</a>
							<a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank" class="share__facebook" title="<?php _e('Delen via Facebook','celebratingdiversity'); ?>">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/cb_icon_facebook.svg" alt="Facebook icon" />
							</a>
						</div>
					</div>
				</div>
				
				<div class="col-sm-5">
					<div class="content content--project">
						<dl>
							<?php if ( get_field( 'project-target' ) ) { ?>
							<dt>Doel</dt>
							<dd><h3><?php the_field( 'project-target' ); ?></h3></dd>
							<?php } ?>
							<?php if ( get_field( 'project-planning' ) ) { ?>
							<dt>Planning</dt>
							<dd><?php the_field( 'project-planning' ); ?></dd>
							<?php } ?>
							<?php if ( get_field( 'project-website' ) ) { ?>
							<dt>Website</dt>
							<dd><a href="<?php the_field( 'project-website' ); ?>" target="_blank"><?php echo str_replace( array('http://','https://'), array('',''), get_field( 'project-website' ) ); ?></a></dd>
							<?php } ?>
							<?php if ( get_field( 'project-contact' ) ) { ?>
							<dt>Contact</dt>
							<dd><?php the_field( 'project-contact' ); ?></dd>
							<?php } ?>
							<?php if ( get_field( 'project-participants' ) ) { ?>
							<dt>Participanten</dt>
							<dd><?php the_field( 'project-participants' ); ?></dd>
							<?php } ?>
						</dl>
					</div>
				</div>
							
			</div>
		</div>
		
	<?php 
	if ( get_field( 'linked_category' ) ) { 
		$activities = get_posts(
			array(
				'post_type' => 'agenda',
				'posts_per_page' => 4,
				'orderby' => 'meta_value_num',
				'meta_key' => 'datum',
				'order' => 'ASC',
				'cat' => get_field( 'linked_category' ),
			)
		);
		
		if ( 0 < count( $activities ) ) {
		?>
		<div class="agenda__single__bottom">
			<div class="wrapper">
				<div class="col-xs-12">
					<h4><?php _e('Gerelateerde activiteiten','celebratingdiversity'); ?></h4>
				</div>
				
				<?php				
				foreach ( $activities as $post ) {
					setup_postdata($post);
					?><div class="col-12 col-sm-6 col-lg-3"><?php
					get_template_part( 'inc/loop', 'agenda-simple');
					?></div><?php
					
				}
				wp_reset_postdata();
				?>
			</div>
		</div>
		<?php 
		}
		
		$articles = get_posts(
			array(
				'post_type' => 'post',
				'posts_per_page' => 2,
				'orderby' => 'rand',
				'cat' => get_field( 'linked_category' ),
			)
		);
		
		if ( 0 < count( $articles ) ) {
		?>
		<div class="project__single__bottom">
			<div class="wrapper">
				<div class="blog__items__homepage">
					<div class="col-xs-12">
						<h4><?php _e('Gerelateerde blogs','celebratingdiversity'); ?></h4>
					</div>
					
					<?php				
					foreach ( $articles as $post ) {
						setup_postdata($post);
						?><div class="col-12 col-sm-6 col-lg-6"><?php
						get_template_part( 'inc/loop', 'blog-default');
						?></div><?php
						
					}
					wp_reset_postdata();
					?>
				</div>
			</div>
		</div>
		<?php 
		}
		
	} 
	?>
	
	</div>
</div>

<?php } ?>

<script type="text/javascript">
	document.getElementById("menu-item-423").className += " current-menu-item";
</script>

<?php
get_footer();
?>